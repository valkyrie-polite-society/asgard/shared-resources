resource "google_kms_key_ring" "asgard" {
  name     = "asgard"
  location = "global"
}

resource "google_kms_crypto_key" "vault" {
  name     = "vault"
  key_ring = google_kms_key_ring.asgard.id
}

resource "google_kms_crypto_key" "boundary" {
  for_each = local.boundary_purposes

  labels   = {}
  name     = "boundary-${each.key}"
  key_ring = google_kms_key_ring.asgard.id
}

locals {
  boundary_purposes = toset(["config", "recovery", "root", "worker-auth"])
}
